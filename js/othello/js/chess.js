class Chess{
  constructor(x, y, idxX, idxY, filled){
    this.x = x;
    this.y = y;
    this.filled = filled;
    this.idxX = idxX;
    this.idxY = idxY;
    this.put = false;
    this.surround = false;
  }



  makeChess(){
    if(this.filled == true)
      fill(255);
    else
      fill(0);
    ellipse(this.x+25, this.y+25, 45);
    this.put = true;
  }



}
