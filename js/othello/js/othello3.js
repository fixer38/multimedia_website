var chess = [];
var surChess = [];
var cntBlack, cntWhite;
var posX, posY;
var newA, newB;
var fin = 0;
var turn = false;
var returnPara = [];

function setup() {
  createCanvas(500, 400);
  background(220);
  for(var i = 0; i < 9; i++){
    stroke(0);
    line(0, 50*i, 400, 50*i);
    line(50*i, 0, 50*i, height);
  }

  for(var i = 0; i < 8; i++){
    chess.push(new Array());
    for(var j = 0; j < 8; j++){
       chess[i].push(new Chess(400/8*i,height/8*j,i,j, true));
    }
  }
  //print(chess.length);
  chess[3][4].filled = false;
  chess[4][3].filled = false;
  noStroke();
  for(var i = 3; i < 5; i++){
    for(var j = 3; j < 5; j++)
      chess[i][j].makeChess();
  }
  cntBlack = 2;
  cntWhite = 2;
}

function draw() {
  textAlign(LEFT);
  noStroke(0);
  fill(220);
  rect(410, 130, 70, 20);
  fill(0);
  textSize(15);
  text("Black: ", 410, 150);
  text(cntBlack, 460, 150);
  fill(220);
  rect(410, 230, 70, 20);
  fill(255);
  text("White: ", 410, 250);
  text(cntWhite, 460, 250);

  if(turn == true){
    fill(220);
    rect(410, 30, 90, 20);
    fill(0);
    text("Now: WHITE", 410, 50);
  }
  else{
    fill(220);
    rect(410, 30, 90, 20);
    fill(0);
    text("Now: BLACK", 410, 50);
  }

  if(cntWhite + cntBlack == 64){
      if(cntWhite > cntBlack){
        textAlign(CENTER);
        textSize(70);
        fill(random(255),random(255),random(255));
        text("White WIN!!!!", 200,200);
      }
      else{
        textAlign(CENTER);
        textSize(70);
        fill(random(255),random(255),random(255));
        text("Black WIN!!!!", 200,200);
      }
  }

}

function mouseClicked(){
  posX = mouseX;
  posY = mouseY;

  if(posX < 400 && posY < height){
    newA = floor(posX/50);
    newB = floor(posY/50);

    if(chess[newA][newB].put == false){
			surChess = checkSur(chess, newA, newB, turn);

      if(surChess.length > 0){
         returnPara = changeColor(chess, chess[newA][newB], surChess, turn);
         if(turn == true && returnPara[1] != 0){
           cntWhite += returnPara[1]+1;
           cntBlack -= returnPara[1];
         }
         else if(turn == false && returnPara[1] != 0){
           cntWhite -= returnPara[1];
           cntBlack += returnPara[1]+1;
         }

         turn = returnPara[0];
      }
    }



  }
}

function checkSur(chess, x, y, turn){
  var surChess = [];

  for(var i = -1; i < 2; i++){
    for(var j = -1; j < 2; j++){
       if(i == 0 && j == 0)
         continue;
       else{
         try{
           if(chess[x+i][y+j].put == true){
             if(turn == false){
               if(chess[x+i][y+j].filled == true)
                 surChess.push(chess[x+i][y+j]);
             }
             else{
               if(chess[x+i][y+j].filled == false)
                 surChess.push(chess[x+i][y+j]);
             }
           }
         }
         catch(err){
           continue;
         }
       }
    }
  }
  //print(surChess[0]);
  return surChess;

}

function changeColor(chess, nowChess, surChess, turn){
  var posX, posY;
  var relPosX, relPosY;
  var colChangeStack = [];
  var changed = false;
  var cnt = 0;
  nowChess.filled = turn;
  for(var i = 0; i < surChess.length; i++){
    posX = nowChess.idxX;
    posY = nowChess.idxY;
    relPosX = surChess[i].idxX - nowChess.idxX;
    relPosY = surChess[i].idxY - nowChess.idxY;
    print(posX, " ", posY, " ", relPosX, " ", relPosY);
    //posX += relPosX;
    //posY += relPosY;
    while(0 <= posX && posX < 8 && 0 <= posY && posY < 8){
       if(chess[posX][posY].put == true){
         if(chess[posX][posY].filled != nowChess.filled){
           colChangeStack.push(chess[posX][posY]);
         }
         else{
           for(var j = 0; j < colChangeStack.length; j++){
             colChangeStack[j].filled = !colChangeStack[j].filled;
             colChangeStack[j].makeChess();
           }
					 changed = true;
           cnt += colChangeStack.length;
           break;
         }
       }
       if(chess[posX][posY].put == false && colChangeStack.length !=0)
         break;
       posX += relPosX;
       posY += relPosY;
      print(colChangeStack.length);
    }
    colChangeStack = [];
  }

  if(changed == true){
    nowChess.makeChess();
    turn = !turn;

  }
  return [turn, cnt];
}

function checkFin(){
  var flag = false;
  if(cntWhite + cntBlack == 64){
    flag = true;
  }
  else{
    for(var i = 0; i < 8; i++){
      for(var j = 0; j < 8; j++){
        if(chess[i][j].put == true)
          continue;
        else{

        }
      }
    }
  }
  if(flag == true){
      if(cntWhite > cntBlack){
        textAlign(CENTER);
        textSize(70);
        fill(random(255),random(255),random(255));
        text("White WIN!!!!", 200,200);
      }
      else{
        textAlign(CENTER);
        textSize(70);
        fill(random(255),random(255),random(255));
        text("Black WIN!!!!", 200,200);
      }
  }
} 
